/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Listados;

import Modelos.Proceso;
import Util.Secuencia;

/**
 *
 * @author anorak
 */
public class ListadoProcesos {

    private Secuencia<Proceso> procesos;

    public ListadoProcesos() {
    }

    @SuppressWarnings("unchecked")
    public ListadoProcesos(Proceso[] procesos) {
        this.procesos = new Secuencia(procesos);
    }

    public Secuencia<Proceso> getProcesos() {
        return this.procesos;
    }

    public void setVideojuegos(Secuencia<Proceso> procesos) {
        this.procesos = procesos;
    }

    public Proceso get(int i) {
        return this.procesos.get(i);
    }

    public void add(int i, Proceso videojuego) {
        this.procesos.add(i, videojuego);
    }

    public void set(int i, Proceso videojuego) {
        this.procesos.set(i, videojuego);
    }

    public int size() {
        return this.procesos.size();
    }

    public Proceso getMenor() {
        if (this.procesos.size() == 0) {
            return null;
        }

        if (this.procesos.size() == 1) {
            return this.procesos.get(0);
        }

        Proceso menor = this.procesos.get(0);
        for (int i = 1; i < this.procesos.size(); i++) {
            if (menor.compareTo(this.procesos.get(i)) > 0) {
                menor = this.procesos.get(i);
            }
        }

        return menor;
    }

    public Proceso getMayor() {
        if (this.procesos.size() == 0) {
            return null;
        }

        if (this.procesos.size() == 1) {
            return this.procesos.get(0);
        }

        Proceso mayor = this.procesos.get(0);
        for (int i = 1; i < this.procesos.size(); i++) {
            if (mayor.compareTo(this.procesos.get(i)) < 0) {
                mayor = this.procesos.get(i);
            }
        }

        return mayor;
    }

    public void borrarRepetidos() {
        Proceso[] tempArray = new Proceso[this.procesos.size()];

        int nuevoTamañoArreglo = 0; 

        for (int i = 0; i < this.procesos.size(); i++) {
            Proceso procesoActual = this.procesos.get(i);
            boolean duplicado = false;
            
            for (int j = 0; !duplicado && j < nuevoTamañoArreglo; j++) {
                if (procesoActual.equals(tempArray[j])) {
                    duplicado = true;
                }
            }
            
            if (!duplicado) {
                tempArray[nuevoTamañoArreglo] = procesoActual;
                nuevoTamañoArreglo++;
            }
        }

        Proceso[] newArray = new Proceso[nuevoTamañoArreglo];
        
        // Pasar los elementos no repetidos del arreglo temporal al nuevo arreglo
        for (int i = 0; i < nuevoTamañoArreglo; i++) {
            newArray[i] = tempArray[i];
        }

        this.procesos = new Secuencia<>(newArray);
    }
    
    private void copiarArregloAOtro(Proceso[] arrOrigen, Proceso[] arrDestino) {
        for (int i = 0; i < arrOrigen.length && i < arrDestino.length; i++) {
            arrDestino[i] = arrOrigen[i];
        }
    }

    @Override
    public String toString() {

        String msg = "";
        for (int i = 0; i < this.procesos.size(); i++) {
            Proceso juego = this.procesos.get(i);
            msg += juego.toString() + "\n";

        }
        return msg;

    }

    public void sort() {
        this.procesos.sort();
    }
}
