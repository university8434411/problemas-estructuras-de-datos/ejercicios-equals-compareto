/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelos;

/**
 *
 * @author anorak
 */
public class Proceso implements Comparable<Proceso>{
    private int idProceso;
    private int prioridad;
    private double usoCPU;
    private double memoria;

    public Proceso() {
    }

    public Proceso(int idProceso, int prioridad, double usoCPU, double memoria) {
        this.idProceso = idProceso;
        this.prioridad = prioridad;
        this.usoCPU = usoCPU;
        this.memoria = memoria;
    }

    @Override
    public String toString() {
        return "Proceso{" + "idProceso=" + idProceso + ", prioridad=" + prioridad + ", usoCPU=" + usoCPU + ", memoria=" + memoria + '}';
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final Proceso other = (Proceso) obj;
        
        if (this.idProceso != other.getIdProceso()) {
            return false;
        }
        if (this.prioridad != other.getPrioridad()) {
            return false;
        }
        
        double resta = Math.abs(this.usoCPU - other.getUsoCPU()); //para evitar restas imprecisas
        double tolerancia = 0.000001; //por si la resta es imprecisa 
        
        return resta < tolerancia;
    }
    

    @Override
    public int compareTo(Proceso o) {
        
        if (this.getIdProceso() < o.getIdProceso()) {
            return -1;
        } else if (this.getIdProceso()> o.getIdProceso()) {
            return 1;
        }

        
        if (this.getPrioridad() < o.getPrioridad()) {
            return -1;
        } else if (this.getPrioridad()> o.getPrioridad()) {
            return 1;
        }

        
        if (this.getUsoCPU() < o.getUsoCPU()) {
            return -1;
        } else if (this.getUsoCPU() > o.getUsoCPU()) {
            return 1;
        }

        return 0;
    }
    
    
    
    
    // ------------ Getters & Setters ------------ //

    public int getIdProceso() {
        return idProceso;
    }

    public void setIdProceso(int idProceso) {
        this.idProceso = idProceso;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public double getUsoCPU() {
        return usoCPU;
    }

    public void setUsoCPU(double usoCPU) {
        this.usoCPU = usoCPU;
    }

    public double getMemoria() {
        return memoria;
    }

    public void setMemoria(double memoria) {
        this.memoria = memoria;
    }
    
}
