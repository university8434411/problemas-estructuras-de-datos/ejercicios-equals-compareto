/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

/**
 * Estructura de datos estática
 *
 * @author josemanuelpr@ufps.edu.co && juandiegosb@ufps.edu.co
 * @param <T>
 */
public class Secuencia<T extends Comparable> {

    // TODO: documentación
    private T[] vector;

    public Secuencia() {
    }

    public Secuencia(T[] vector) {
        this.vector = vector;
    }

    //Crear:
    @SuppressWarnings("unchecked")
    public Secuencia(int n) {
        if (n <= 0) {
            throw new RuntimeException("Error dato debe ser >0:" + n);
        }
        Comparable temporal[] = new Comparable[n];
        this.vector = (T[]) temporal;
    }

    //Insertar:
    public void add(int i, T elemento) {
        this.validar(i);
        this.vector[i] = elemento;
    }

    public void set(int i, T elemento) {
        this.add(i, elemento);
    }

    public T get(int i) {
        this.validar(i);
        return this.vector[i];
    }

    public int size() {
        return this.vector.length;
    }

    private void validar(int i) {
        if (i < 0 || i >= this.vector.length) {
            throw new RuntimeException("Error dato fuera de rango:" + i);
        }
    }
    
    /**
     * Método de ordenamiento para la secuencia, implementa Bubble Sort.
     */
    public void sort() {

        T ArrayN[] = this.vector;
        
        for (int i = 0; i < ArrayN.length - 1; i++) {
            for (int j = 0; j < ArrayN.length - 1; j++) {
                
                @SuppressWarnings("unchecked")
                int c = ArrayN[j].compareTo(ArrayN[j + 1]);
                
                if (c > 0) {
                    T temp = ArrayN[j + 1];
                    ArrayN[j + 1] = ArrayN[j];
                    ArrayN[j] = temp;
                    
                }
            }
        }

    }

    private boolean esVacio() {
        return this.vector == null || this.vector.length == 0;
    }

    /**
     * Obtiene el arreglo de elementos de esta secuencia.
     *
     * @return El arreglo de elementos de esta secuencia.
     */
    public T[] getVector() {
        return vector;
    }

    /**
     * Devuelve una representación de cadena de esta secuencia.
     *
     * @return Una cadena que contiene los elementos de esta secuencia,
     * separados por una nueva línea. Si la secuencia está vacía, devuelve una
     * cadena vacía.
     */
    @Override
    public String toString() {
        String msg = "";
        if (!this.esVacio()) {
            for (T t : vector) {
                msg += (t.toString()) + ("\n");
            }
        }
        return msg;
    }
}
