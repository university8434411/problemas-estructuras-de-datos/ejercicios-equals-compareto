# Ejercicios Equals-CompareTo



## Getting started

Acá realizo varios de los ejercicios de práctica, que están en este [documento](https://docs.google.com/document/d/14yuyVxoNQmVdFMEL_WTpBvqiFrYY8hn8GTlN1Jrieto/edit).

Lo dejo en público para poder clonar el contenido en cualquier IDE, y no tener problemas de autenticación ni nada de eso.

Acá un pequeño comando para la terminal, asi se copia a una carpeta llamada etc en el escritorio para no andar configurando tantas cosas.

```
cd Desktop && mkdir etc && cd etc && git clone https://gitlab.com/university8434411/problemas-estructuras-de-datos/ejercicios-equals-compareto.git

```